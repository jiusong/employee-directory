# Employee Directory

A Block(Square) take-home project.

## Build tools & versions used
Android Studio Chipmunk | 2021.2.1 Patch 1
Gradle: 7.2.1

## Steps to run the app
Install Android Studio Chipmunk;
Open Project from Git or local folder;
Build and install app.
Use option menus to choose different end points for employee source.
Pull to refresh list.

## What areas of the app did you focus on?
I use this project to learn & practice Android architecture with Jetpack components.
Tried to follow best practices Google recommends, 
utilizing Coroutine, Flow to make network requests;
ViewModel & LiveData to handle Android lifecycles;
Navigation components to design UI.
Hilt for dependency injection.

## What was the reason for your focus? What problems were you trying to solve?
It's easier to make architecture decisions with a brand new project, as no need to backward-compatible with legacy code.
App architecture is MVVM:
View --> ViewModel[LiveData] --> Repository[Flow] ---> Remote Data Source[Retrofit]
                                                  ---> Local Data Source[Room] (Not implemented yet)
Some of the problems I was trying to solve are:
How to use Coroutine & Flow make network requests?
How to test network service with Mockwebserver?
How to handle network error?
StateFlow or LiveData for updating UI?
Hilt dependency Injection for network service, ViewModel, Application Context.

## How long did you spend on this project?
I spent about 6~7 hours working this projects.
I spent quite some time to search and compare what the best way to handle network request error while using coroutine & flow.
Also sometime to understand StateFlow and compare it with LiveData.

## Did you make any trade-offs for this project? What would you have done differently with more time? 
Yeah, like the choice of StateFlow or LiveData, seems to me LiveData wins for its simplicity and easy of use.
StateFlow might be useful for more complicated cases, but I don't see a strong point to use it for now.
I mostly focused on Architecture part, so the UI is very simple or primitive. 
If I have more time I would like to do some more polish for the employee list, like group them by team with a group header.
Error handling is also very basic, just displaying a toast, would need a more user friendly version to make it production ready.
Or adding more tests, like testing ViewModel, UI.

## What do you think is the weakest part of your project?
UI is probably the least polished part, it's just completed the basic functionalities.

## Did you copy any code or dependencies? Please make sure to attribute them here!
I didn't copy much code directly, but I did refer those following links:
Kotlin FLow on Android -- Quick guide: 
https://proandroiddev.com/kotlin-flow-on-android-quick-guide-76667e872166
Migrating from LiveData to Kotlin’s Flow:
https://medium.com/androiddevelopers/migrating-from-livedata-to-kotlins-flow-379292f419fb
Retrofit — Effective error handling with Kotlin Coroutine and Result API: 
https://blog.canopas.com/retrofit-effective-error-handling-with-kotlin-coroutine-and-result-api-405217e9a73d
Android Architecture: MVVM with Coroutines + Retrofit + Hilt + Kotlin Flow + Room:
https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
Testing Kotlin flows on Android:
https://developer.android.com/kotlin/flow/test

## Is there any other information you’d like us to know?
I can think of several aspects need to improved for making the code production ready:
Provides authentication token as a header for all network request.
UI should be able scale well with different font size, extra long text.
Proper styling used for UI.