package com.jiusong.employee.data

import com.jiusong.employee.data.model.Employee
import com.jiusong.employee.data.network.EmployeeService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

import javax.inject.Inject

class EmployeeRepository @Inject constructor(private val apiService: EmployeeService) {

    fun getEmployees(json: String): Flow<List<Employee>> {
        return flow {
            val res = apiService.getEmployees(json)
            emit(res.employees)
        }
    }
}