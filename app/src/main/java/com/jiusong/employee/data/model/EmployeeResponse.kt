package com.jiusong.employee.data.model

data class EmployeeResponse(
    val employees: List<Employee>
)