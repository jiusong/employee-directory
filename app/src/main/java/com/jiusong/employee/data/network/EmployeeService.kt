package com.jiusong.employee.data.network

import com.jiusong.employee.data.model.EmployeeResponse
import retrofit2.http.GET
import retrofit2.http.Path


interface EmployeeService {

    @GET("sq-mobile-interview/{json}")
    suspend fun getEmployees(@Path("json") json: String): EmployeeResponse
}