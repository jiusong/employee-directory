package com.jiusong.employee.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jiusong.employee.data.EmployeeRepository
import com.jiusong.employee.data.model.Employee
import com.jiusong.employee.data.model.Result
import com.jiusong.employee.util.AppConstants
import com.jiusong.employee.util.EmployeeValidation
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EmployeeViewModel @Inject constructor(private val repository: EmployeeRepository) : ViewModel() {

    private val _employees = MutableLiveData<Result<List<Employee>>>()
    val employees: LiveData<Result<List<Employee>>> = _employees

    private var employeeSource: String = AppConstants.EMPLOYEES_JSON

    fun loadEmployees() {
        if (employees.value?.status != Result.Status.SUCCESS) {
            getEmployees()
        }
    }

    fun getEmployees() {
        viewModelScope.launch {
            repository.getEmployees(employeeSource)
                .onStart { _employees.value = Result.loading() }
                .catch { error ->
                    _employees.value = Result.error(error.message, error) }
                .collect {
                    collectEmployees(it)
                }
        }
    }

    private fun collectEmployees(list: List<Employee>) {
        val res = EmployeeValidation.getValidEmployees(list)
        val sorted = res.sortedBy { employee -> employee.team }
        _employees.value = Result.success(sorted)
    }

    fun setEmployeeSource(source: String) {
        if (employeeSource != source) {
            employeeSource = source
            getEmployees()
        }
    }
}