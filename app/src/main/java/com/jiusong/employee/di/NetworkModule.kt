package com.jiusong.employee.di

import android.content.Context
import com.jiusong.employee.R
import com.jiusong.employee.data.network.EmployeeService
import com.jiusong.employee.data.network.EmployeeServiceFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun providesEmployeeService(@ApplicationContext context: Context): EmployeeService {
        return EmployeeServiceFactory.retrofit(context.getString(R.string.base_url))
            .create(EmployeeService::class.java)
    }
}