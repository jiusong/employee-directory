package com.jiusong.employee.util

object AppConstants {
    const val EMPLOYEES_JSON = "employees.json"
    const val EMPLOYEES_MALFORMED_JSON = "employees_malformed.json"
    const val EMPLOYEES_EMPTY_JSON = "employees_empty.json"
}