package com.jiusong.employee.util

import androidx.annotation.VisibleForTesting
import com.jiusong.employee.data.model.Employee

object EmployeeValidation {

    /**
     * Get a list of valid Employees, if one of them is malformed, we assume the entire list is invalid.
     *
     * @param list
     * @return
     */
    fun getValidEmployees(list: List<Employee>): List<Employee> {
        list.forEach {
            if (isMalformed(it)) return emptyList()
        }
        return list
    }


    /**
     * Check required properties in an Employee to see if it's malformed.
     * Required properties: uuid, full_name, email_address, team, employee_type.
     *
     * @param employee
     * @return
     */
    @VisibleForTesting
    fun isMalformed(employee: Employee): Boolean {
        return employee.uuid.isNullOrEmpty()
                || employee.full_name.isNullOrEmpty()
                || employee.email_address.isNullOrEmpty()
                || employee.team.isNullOrEmpty()
                || employee.team.isNullOrEmpty()
    }
}