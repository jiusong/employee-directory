package com.jiusong.employee.ui

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.jiusong.employee.R
import com.jiusong.employee.data.model.Employee
import com.jiusong.employee.data.model.Result
import com.jiusong.employee.databinding.FragmentEmployeeListBinding
import com.jiusong.employee.ui.adapter.EmployeeAdapter
import com.jiusong.employee.util.AppConstants
import com.jiusong.employee.viewmodel.EmployeeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EmployeeListFragment : Fragment(R.layout.fragment_employee_list) {

    private lateinit var binding: FragmentEmployeeListBinding
    private val viewModel: EmployeeViewModel by activityViewModels()
    @Inject
    lateinit var employeeAdapter: EmployeeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentEmployeeListBinding.bind(view)
        setupToolBar()
        setupEmployeeList()
        viewModel.loadEmployees()
    }

    private fun setupToolBar() {
        binding.toolBar.title = getString(R.string.app_name)
        (activity as AppCompatActivity).setSupportActionBar(binding.toolBar)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.source_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.valid -> {
                viewModel.setEmployeeSource(AppConstants.EMPLOYEES_JSON)
                true
            }
            R.id.malformed -> {
                viewModel.setEmployeeSource(AppConstants.EMPLOYEES_MALFORMED_JSON)
                true
            }
            R.id.empty -> {
                viewModel.setEmployeeSource(AppConstants.EMPLOYEES_EMPTY_JSON)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun setupEmployeeList() {
        val viewManager = LinearLayoutManager(requireContext())
        binding.employeeList.apply {
            layoutManager = viewManager
            adapter = employeeAdapter
        }
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.getEmployees()
        }
        subscribeEmployees()
    }

    private fun subscribeEmployees() {
        viewModel.employees.observe(viewLifecycleOwner) { res ->
            when (res.status) {
                Result.Status.SUCCESS -> {
                    binding.swipeRefresh.isRefreshing = false
                    binding.progressBar.visibility = View.GONE
                    handleEmployeeResult(res.data)
                }
                Result.Status.LOADING -> {
                    binding.swipeRefresh.isRefreshing = false
                    binding.progressBar.visibility = View.VISIBLE
                }
                Result.Status.ERROR -> {
                    binding.swipeRefresh.isRefreshing = false
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), res.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun handleEmployeeResult(employeeList: List<Employee>?) {
        employeeAdapter.submitList(employeeList)
        if (employeeList.isNullOrEmpty()) {
            binding.emptyState.root.visibility = View.VISIBLE
        } else {
            binding.emptyState.root.visibility = View.GONE
        }
    }
}