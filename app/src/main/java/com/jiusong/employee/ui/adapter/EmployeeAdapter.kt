package com.jiusong.employee.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jiusong.employee.R
import com.jiusong.employee.data.model.Employee
import com.jiusong.employee.databinding.EmployeeItemBinding
import javax.inject.Inject


class EmployeeAdapter @Inject constructor() :
    ListAdapter<Employee, EmployeeAdapter.ViewHolder>(EmployeeDiffCallback()) {

    inner class ViewHolder(private val binding: EmployeeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(employee: Employee) {
            binding.name.text = employee.full_name
            binding.team.text = binding.team.context.getString(R.string.employee_team, employee.team)
            binding.biography.text = employee.biography

            Glide.with(binding.imageView)
                .load(employee.photo_url_small)
                .centerCrop()
                .placeholder(R.drawable.ic_profile_default)
                .into(binding.imageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = EmployeeItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val employee = getItem(position)
        holder.bind(employee)
    }
}