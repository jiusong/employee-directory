package com.jiusong.employee.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.jiusong.employee.data.model.Employee

class EmployeeDiffCallback(): DiffUtil.ItemCallback<Employee>() {
    override fun areItemsTheSame(oldItem: Employee, newItem: Employee): Boolean {
        return oldItem.uuid == newItem.uuid
    }

    override fun areContentsTheSame(oldItem: Employee, newItem: Employee): Boolean {
        return oldItem == newItem
    }
}