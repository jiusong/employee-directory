package com.jiusong.employee.data

import com.jiusong.employee.data.network.EmployeeService
import com.jiusong.employee.data.network.EmployeeServiceFactory
import com.jiusong.employee.util.EmployeeValidation
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import util.FileReader
import java.net.HttpURLConnection

class EmployeeRepositoryTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var repository: EmployeeRepository
    private lateinit var employeeService: EmployeeService

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        employeeService = EmployeeServiceFactory.retrofit(mockWebServer.url("/").toString())
            .create(EmployeeService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test fetch & parse valid employees`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(FileReader("employees_valid.json").content)
        mockWebServer.enqueue(response)
        repository = EmployeeRepository(employeeService)
        // Act
        runBlocking {
            repository.getEmployees("").collect {
                // Assert
                assertEquals("Justine Mason", it.first().full_name)
                assertEquals("Jack Dorsey", it.last().full_name)
            }
        }
    }

    @Test
    fun `test fetch & validate malformed employees`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(FileReader("employees_malformed.json").content)
        mockWebServer.enqueue(response)
        repository = EmployeeRepository(employeeService)
        // Act
        runBlocking {
            repository.getEmployees("").collect {
                // Assert
                val validEmployees = EmployeeValidation.getValidEmployees(it)
                assertTrue(validEmployees.isEmpty())
            }
        }
    }

    @Test
    fun `test fetch empty employees`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(FileReader("employees_empty.json").content)
        mockWebServer.enqueue(response)
        repository = EmployeeRepository(employeeService)
        // Act
        runBlocking {
            repository.getEmployees("").collect {
                // Assert
                assertTrue(it.isEmpty())
            }
        }
    }
}