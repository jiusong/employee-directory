package com.jiusong.employee.util

import com.google.gson.Gson
import com.jiusong.employee.data.model.Employee
import com.jiusong.employee.data.model.EmployeeResponse
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import util.FileReader

class EmployeeValidationTest {

    private lateinit var employees: List<Employee>

    @Before
    fun setUp() {
        val json = FileReader("employees_malformed.json").content
        employees = Gson().fromJson(json, EmployeeResponse::class.java).employees
    }

    @Test
    fun `test valid employee`() {
        assertFalse(EmployeeValidation.isMalformed(employees.first()))
    }

    @Test
    fun `test malformed employee`() {
        assertTrue(EmployeeValidation.isMalformed(employees.last()))
    }
}