package util

import java.io.InputStreamReader

open class FileReader(path: String) {

    val content: String

    init {
        val reader = InputStreamReader(javaClass.classLoader!!.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }
}
